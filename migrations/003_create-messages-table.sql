CREATE TABLE IF NOT EXISTS messages
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    "roomId" bigint NOT NULL,
    "senderId" bigint NOT NULL,
    message text NOT NULL,
    "createdAt" timestamp without time zone NOT NULL DEFAULT now(),
    CONSTRAINT "messages_roomId_fkey" FOREIGN KEY ("roomId") REFERENCES rooms (id),
    CONSTRAINT "messages_senderId_fkey" FOREIGN KEY ("senderId") REFERENCES users (id)
);
