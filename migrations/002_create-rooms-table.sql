CREATE TABLE IF NOT EXISTS rooms
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    name text NOT NULL,
    description text NOT NULL,
    password text NOT NULL
);
