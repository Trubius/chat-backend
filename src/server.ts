import bcrypt from 'bcrypt';
import fastify, { FastifyInstance } from 'fastify';
import cors from 'fastify-cors';
import oas from 'fastify-oas';
import passport from 'fastify-passport';
import fastifySecureSession from 'fastify-secure-session';
import fastifySocketIO from 'fastify-socket.io';
import passportLocal from 'passport-local';
import path from 'path';
import { createDb, migrate } from 'postgres-migrations';
import { Server } from 'socket.io';
import { config } from './config';
import { createPool } from './database/dbconfig';
import { User } from './interfaces/user';
import { healthRouter } from './modules/health/routes';
import { createMessagesRepository } from './modules/messages/repository';
import { createMessagesRouter } from './modules/messages/routes';
import { createRoomsRepository } from './modules/rooms/repository';
import { createRoomsRouter } from './modules/rooms/routes';
import { createUsersRepository } from './modules/users/repository';
import { createUsersRouter } from './modules/users/routes';
import { createActiveUsersHandler } from './utils/active-users-handler';

const ONE_DAY = 86400000;

export const createServer = async (): Promise<FastifyInstance> => {
  const server = fastify({ logger: true });

  server.setErrorHandler((error, request, response) => {
    request.log.error(error.message);
    void response.status(500).send({ error: 'INTERNAL_SERVER_ERROR' });
  });

  const pool = createPool(config.database.connectionString);

  const client = await pool.connect();
  try {
    await createDb(config.databaseName, { client });
    await migrate({ client }, path.join(__dirname, '..', 'migrations'));
  } catch (error) {
    console.error(error);
    process.exit(1);
  } finally {
    client.release();
  }

  void server.register(cors, { origin: config.frontendUrl });
  void server.register(fastifySocketIO, {cors: {origin: config.frontendUrl}});

  void server.register(oas, {
    routePrefix: '/documentation',
    exposeRoute: true,
    swagger: {
      info: {
        title: 'Chat API',
        description: 'API documentation',
        version: '0.1.0',
      },
      externalDocs: {
        url: 'https://swagger.io',
        description: 'Find more info here',
      },
      servers: [{ url: config.backendUrl }],
      schemes: ['http'],
      consumes: ['application/json'],
      produces: ['application/json'],
      tags: [
        { name: 'health', description: 'Server related end-points' },
        { name: 'rooms', description: 'Room related end-points' },
        { name: 'messages', description: 'Messages related end-points' },
        { name: 'users', description: 'User related end-points' },
      ],
    },
  });

  const roomsRepository = createRoomsRepository(pool);
  const messagesRepository = createMessagesRepository(pool);
  const usersRepository = createUsersRepository(pool);
  const roomsRouter = createRoomsRouter(roomsRepository);
  const messagesRouter = createMessagesRouter(messagesRepository);
  const usersRouter = createUsersRouter(usersRepository);

  passport.use(
    'local',
    new passportLocal.Strategy(
      { usernameField: 'email', passwordField: 'password' },

      // login method
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      async (email, password, done) => {
        const user = await usersRepository.getUserByEmail(email);
        if (!user) {
          return done(null, false, { message: 'INSUFFICIENT_PERMISSION' });
        }
        const { salt, passwordHash } = user;
        const comparePassword = await bcrypt.hash(password, salt).catch(() => null);
        if (passwordHash !== comparePassword) {
          return done(null, false, { message: 'INSUFFICIENT_PERMISSION' });
        }
        return done(null, user);
      },
    ),
  );

  void passport.registerUserSerializer(async (user: User) => user.id);
  void passport.registerUserDeserializer(async (id: string) => usersRepository.getUserById(id));

  void server.register(fastifySecureSession, {
    key: Buffer.from(config.secureSessionKey, 'hex'),
    cookie: { path: '/', maxAge: ONE_DAY },
    cookieName: 'chatCookie',
  });
  void server.register(passport.initialize());
  void server.register(passport.secureSession());

  void server.register(usersRouter, { prefix: '/api/users' });
  void server.register(healthRouter, { prefix: '/api/health' });
  void server.register(roomsRouter, { prefix: '/api/rooms' });
  void server.register(messagesRouter, { prefix: '/api/rooms' });

  const activeUsersHandler = createActiveUsersHandler();

  server.ready().then(() => {
    const io = server.io as Server;

    io.on('connect', socket => {
      socket.on('login', ({userId}: {userId: string}) => {
        activeUsersHandler.addSocket(socket.id, userId);
        const activeUserIds = activeUsersHandler.getActiveUserIds();
        io.emit('activeUserIds', activeUserIds);
      }),

      socket.on('logout', () => {
        activeUsersHandler.removeSocket(socket.id);
        const activeUserIds = activeUsersHandler.getActiveUserIds();
        io.emit('activeUserIds', activeUserIds);
      }),

      socket.on('disconnect', () => {
        activeUsersHandler.removeSocket(socket.id);
        const activeUserIds = activeUsersHandler.getActiveUserIds();
        io.emit('activeUserIds', activeUserIds);
      })

    })
  })

  return server;
};
