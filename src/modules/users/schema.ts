import { RouteSchema } from '../../interfaces/route-schema';

export const userSchema = {
  type: 'object',
  properties: {
    id: { type: 'string' },
    name: { type: 'string' },
    email: { type: 'string' },
  },
};

export const getUsersSchema: RouteSchema = {
  summary: 'get users',
  description: 'get users',
  tags: ['users'],
  response: {
    200: {
      type: 'array',
      items: userSchema,
    },
    404: {
      type: 'object',
      properties: { error: { type: 'string' } },
    },
  },
};

export const getLoggedInUserSchema: RouteSchema = {
  summary: 'get logged-in user',
  description: 'get logged-in user',
  tags: ['users'],
  response: {
    200: userSchema,
    404: {
      type: 'object',
      properties: { error: { type: 'string' } },
    },
  },
};

export const getUserSchema: RouteSchema = {
  summary: 'get user',
  description: 'get user',
  tags: ['users'],
  params: {
    type: 'object',
    required: ['id'],
    properties: {
      id: { type: 'string' },
    },
  },
  response: {
    200: userSchema,
    404: {
      type: 'object',
      properties: { error: { type: 'string' } },
    },
  },
};

export const createUserSchema: RouteSchema = {
  summary: 'create user',
  description: 'create user',
  tags: ['users'],
  body: {
    type: 'object',
    required: ['email', 'password'],
    properties: {
      name: { type: 'string' },
      email: { type: 'string' },
      password: { type: 'string' },
    },
  },
  response: {
    201: {
      type: 'object',
      properties: { status: { type: 'string' } },
    },
    404: {
      type: 'object',
      properties: { error: { type: 'string' } },
    },
  },
};

export const loginUserSchema: RouteSchema = {
  summary: 'login user',
  description: 'login user',
  tags: ['users'],
  body: {
    type: 'object',
    required: ['email', 'password'],
    properties: {
      email: { type: 'string' },
      password: { type: 'string' },
    },
  },
  response: {
    200: {
      type: 'object',
      properties: { status: { type: 'string' } },
    },
    401: {
      type: 'object',
      properties: { error: { type: 'string' } },
    },
  },
};

export const logoutUserSchema: RouteSchema = {
  summary: 'logout user',
  description: 'logout user',
  tags: ['users'],
  response: {
    200: {
      type: 'object',
      properties: { status: { type: 'string' } },
    },
  },
};
