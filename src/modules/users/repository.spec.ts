import bcrypt from 'bcrypt';
import { config } from '../../config';
import { createPool } from '../../database/dbconfig';
import { closeTestDatabase, createTestDatabase, resetTestDatabase } from '../../utils/test-database.utils';
import { createUsersRepository, UsersRepository } from './repository';

describe('User Repository', () => {
  const pool = createPool(config.testDatabase.connectionString);
  let usersRepository: UsersRepository = createUsersRepository(pool);

  beforeAll(async () => {
    await resetTestDatabase(pool);
    await createTestDatabase(pool);
  });

  afterAll(async () => {
    await closeTestDatabase(pool);
  });

  it('should connect to the database and create admin user', async () => {
    const result = await usersRepository.list();
    expect(result.length).toBe(1);
    expect(result[0]).toHaveProperty('email', 'admin@admin.com');
    expect(result[0]).toHaveProperty('name', 'admin');
  });

  it('should create a new user', async () => {
    const name = 'test-user';
    const email = 'test@test.com';
    const password = 'pass123';
    const salt = await bcrypt.genSalt(10);
    const passwordHash = await bcrypt.hash(password, salt);
    await usersRepository.createUser(name, email, salt, passwordHash);
    const testUser = await usersRepository.getUserByEmail(email);
    expect(testUser).toHaveProperty('email', 'test@test.com');
    expect(testUser).toHaveProperty('name', 'test-user');
  });

  it('should find admin user by id', async () => {
    const adminUser = await usersRepository.getUserById('1');
    expect(adminUser).toHaveProperty('email', 'admin@admin.com');
    expect(adminUser).toHaveProperty('name', 'admin');
  });
})
