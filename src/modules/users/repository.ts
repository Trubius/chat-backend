import { Pool } from 'pg';
import { User } from '../../interfaces/user';

export type UsersRepository = {
  list: () => Promise<User[]>;
  getUserById: (id: string) => Promise<User>;
  getUserByEmail: (email: string) => Promise<User>;
  createUser: (name: string, email: string, salt: string, passwordHash: string) => Promise<void>;
};

export const createUsersRepository = (pool: Pool): UsersRepository => {
  return {
    list: async () => {
      const sql = `SELECT "id", "name", "email" FROM "users";`;
      const res = await pool.query<User>(sql);
      return res.rows;
    },
    getUserById: async id => {
      const sql = `SELECT "id", "name", "email" FROM "users" u WHERE u."id" = $1;`;
      const res = await pool.query<User>(sql, [id]);
      return res.rows[0];
    },
    getUserByEmail: async email => {
      const sql = `SELECT * FROM "users" u WHERE u."email" = $1;`;
      const res = await pool.query<User>(sql, [email]);
      return res.rows[0];
    },
    createUser: async (name, email, salt, passwordHash) => {
      const sql = `INSERT INTO "users" ("name", "email", "salt", "passwordHash") VALUES ($1, $2, $3, $4);`;
      await pool.query(sql, [name, email, salt, passwordHash]);
    },
  };
};
