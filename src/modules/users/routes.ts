import bcrypt from 'bcrypt';
import { FastifyPluginCallback } from 'fastify';
import passport from 'fastify-passport';
import { Session } from 'fastify-secure-session';
import { isAuthenticated } from '../../auth/is-authenticated-mw';
import { User } from '../../interfaces/user';
import { UsersRepository } from './repository';
import { createUserSchema, getLoggedInUserSchema, getUserSchema, getUsersSchema, loginUserSchema, logoutUserSchema } from './schema';

export const createUsersRouter = (usersRepository: UsersRepository): FastifyPluginCallback => {
  return (server, _, done) => {
    server.get<{ Params: { id: string } }>(
      '/', { preValidation: isAuthenticated, schema: getUsersSchema },
      async (_, response) => {
        const result = await usersRepository.list();
        return response.code(200).send(result);
      },
    );

    server.get<{ Params: { id: string } }>(
      '/me',
      { preValidation: isAuthenticated, schema: getLoggedInUserSchema },
      async (request, response) => {
        const { id } = request.user as User;
        const result = await usersRepository.getUserById(id);
        return response.code(200).send(result);
      },
    );

    server.get<{ Params: { id: string } }>(
      '/:id',
      { preValidation: isAuthenticated, schema: getUserSchema },
      async (request, response) => {
        const { id } = request.params;
        const result = await usersRepository.getUserById(id);
        return response.code(200).send(result);
      },
    );

    server.post<{ Body: { name: string; email: string; password: string } }>(
      '/signup',
      { schema: createUserSchema },
      async (request, response) => {
        const { name, email, password } = request.body;
        const salt = await bcrypt.genSalt(10);
        const passwordHash = await bcrypt.hash(password, salt);
        await usersRepository.createUser(name, email, salt, passwordHash);
        return response.code(201).send({ status: 'REGISTRATION_COMPLETE' });
      },
    );

    server.post<{ Body: { email: string; password: string } }>(
      '/login',
      { preHandler: passport.authenticate('local', { authInfo: false }), schema: loginUserSchema },
      async (_, response) => {
        return response.code(200).send({ status: 'LOGGED_IN' });
      },
    );

    server.post('/logout', { schema: logoutUserSchema }, async (request, response) => {
      // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
      (request.session as Session).delete();
      return response.code(200).send({ status: 'LOGGED_OUT' });
    });

    done();
  };
};
