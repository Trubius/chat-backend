import { FastifyPluginCallback } from 'fastify';
import { isAuthenticated } from '../../auth/is-authenticated-mw';
import { MessagesRepository } from './repository';
import { createMessageSchema } from './schema';

export const createMessagesRouter = (messagesRepository: MessagesRepository): FastifyPluginCallback => {
  return (server, _, done) => {
    server.post<{ Params: { roomId: string }; Body: { senderId: string, message: string } }>(
      '/:roomId/messages',
      { preValidation: isAuthenticated, schema: createMessageSchema },
      async (request, response) => {
        if (!request.body) {
          return response.code(400).send({ error: 'INVALID_REQUEST' });
        }
        const { roomId } = request.params;
        // check error message, and add it to setErrorHandler
        const { senderId, message } = request.body;
        const result = await messagesRepository.create(roomId, senderId, message);
        server.io.emit(`chat-message-${roomId}`, result);
        return response.code(201).send(result);
      },
    );

    done();
  };
};
