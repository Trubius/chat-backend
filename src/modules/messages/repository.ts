import { Pool } from 'pg';
import { Message } from '../../interfaces/message';

export type MessagesRepository = {
  create: (roomId: string, senderId: string, message: string) => Promise<Message>;
};

export const createMessagesRepository = (pool: Pool): MessagesRepository => {
  return {
    create: async (roomId, senderId, message) => {
      const sql = `WITH inserted as (
        INSERT INTO "messages" ("roomId", "senderId", "message")
        VALUES ($1, $2, $3) RETURNING *
        )
        SELECT inserted."id", inserted."message", inserted."createdAt",
        json_build_object('id', u.id, 'name', u.name, 'email', u.email) sender
        FROM inserted
        INNER JOIN "users" u
        ON u."id" = inserted."senderId";`;
      const res = await pool.query<Message>(sql, [roomId, senderId, message]);
      return res.rows[0];
    },
  };
};
