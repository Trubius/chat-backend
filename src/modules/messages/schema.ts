import { RouteSchema } from '../../interfaces/route-schema';
import { userSchema } from '../users/schema';

export const messageSchema = {
  type: 'object',
  properties: {
    id: { type: 'string' },
    message: { type: 'string' },
    createdAt: { type: 'string', format: 'date-time' },
    sender: userSchema,
  },
};

export const createMessageSchema: RouteSchema = {
  summary: 'create message',
  description: 'create message',
  tags: ['messages'],
  params: {
    type: 'object',
    required: ['roomId'],
    properties: {
      roomId: { type: 'string' },
    },
  },
  body: {
    type: 'object',
    required: ['senderId', 'message'],
    properties: {
      senderId: { type: 'string', default: '' },
      message: { type: 'string', default: '' },
    },
  },
  response: {
    201: messageSchema,
    404: {
      type: 'object',
      properties: { error: { type: 'string' } },
    },
  },
};
