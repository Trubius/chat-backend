import { RouteSchema } from '../../interfaces/route-schema';

export const getHealthSchema: RouteSchema = {
  summary: 'health check',
  description: 'health check',
  tags: ['health'],
  response: {
    200: {
      type: 'object',
      properties: {
        status: {
          type: 'string',
        },
      },
    },
  },
};
