import { FastifyPluginCallback } from 'fastify';
import { getHealthSchema } from './schema';

export const healthRouter: FastifyPluginCallback = (server, _, done) => {
  server.get('/', { schema: getHealthSchema }, async (_, response) => {
    return response.code(200).send({ status: 'OK' });
  });

  done();
};
