import { FastifyPluginCallback } from 'fastify';
import { isAuthenticated } from '../../auth/is-authenticated-mw';
import { RoomsRepository } from './repository';
import { createRoomSchema, getRoomSchema, listRoomsSchema } from './schema';

export const createRoomsRouter = (roomsRepository: RoomsRepository): FastifyPluginCallback => {
  return (server, _, done) => {
    server.get('/', { preValidation: isAuthenticated, schema: listRoomsSchema }, async (_, response) => {
      const result = await roomsRepository.list();
      return response.code(200).send(result);
    });

    server.get<{ Params: { roomId: string } }>(
      '/:roomId',
      { preValidation: isAuthenticated, schema: getRoomSchema },
      async (request, response) => {
        const { roomId } = request.params;
        const result = await roomsRepository.get(roomId);
        if (!result) {
          return response.code(404).send({ error: 'ROOM_NOT_FOUND' });
        }
        return response.code(200).send(result);
      },
    );

    server.post<{ Body: { name: string; description?: string; password?: string } }>(
      '/',
      { preValidation: isAuthenticated, schema: createRoomSchema },
      async (request, response) => {
        if (!request.body) {
          return response.code(400).send({ error: 'INVALID_REQUEST' });
        }
        const { name, description, password } = request.body;
        const result = await roomsRepository.create(name, description, password);
        return response.code(201).send(result);
      },
    );

    done();
  };
};
