import { Pool } from 'pg';
import { Room, RoomWithMessages } from '../../interfaces/room';

export type RoomsRepository = {
  list: () => Promise<Room[]>;
  get: (id: string) => Promise<RoomWithMessages>;
  create: (name: string, description?: string, password?: string) => Promise<Room>;
};

export const createRoomsRepository = (pool: Pool): RoomsRepository => {
  return {
    list: async () => {
      const sql = 'SELECT * FROM "rooms";';
      const res = await pool.query<Room>(sql);
      return res.rows;
    },
    get: async id => {
      const sql = `
      SELECT r.*, COALESCE(jsonb_agg(to_jsonb(m) - 'roomId') FILTER (WHERE m."id" IS NOT NULL), '[]') messages
      FROM "rooms" r
      LEFT JOIN "messages" m
      ON m."roomId"=r."id"
      WHERE r."id"=$1
      GROUP BY r."id";`;
      const res = await pool.query<RoomWithMessages>(sql, [id]);
      return res.rows[0];
    },
    create: async (name, description, password) => {
      const sql = 'INSERT INTO "rooms" ("name", "description", "password") VALUES ($1, $2, $3) RETURNING *;';
      const res = await pool.query<Room>(sql, [name, description, password]);
      return res.rows[0];
    },
  };
};
