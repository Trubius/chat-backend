import { RouteSchema } from '../../interfaces/route-schema';
import { messageSchema } from '../messages/schema';

export const roomSchema = {
  type: 'object',
  properties: {
    id: { type: 'string' },
    name: { type: 'string' },
    description: { type: 'string' },
    password: { type: 'string' },
  },
};

export const roomWithMessagesSchema = {
  type: 'object',
  properties: {
    id: { type: 'string' },
    name: { type: 'string' },
    description: { type: 'string' },
    password: { type: 'string' },
    messages: {
      type: 'array',
      items: messageSchema,
    },
  },
};

export const listRoomsSchema: RouteSchema = {
  summary: 'list rooms',
  description: 'list rooms',
  tags: ['rooms'],
  response: {
    200: {
      type: 'array',
      items: roomSchema,
    },
  },
};

export const getRoomSchema: RouteSchema = {
  summary: 'room details',
  description: 'room details',
  tags: ['rooms'],
  params: {
    type: 'object',
    required: ['roomId'],
    properties: {
      roomId: { type: 'string' },
    },
  },
  response: {
    200: roomWithMessagesSchema,
    404: {
      type: 'object',
      properties: { error: { type: 'string' } },
    },
  },
};

export const createRoomSchema: RouteSchema = {
  summary: 'create room',
  description: 'create room',
  tags: ['rooms'],
  body: {
    type: 'object',
    required: ['name'],
    properties: {
      name: { type: 'string', default: 'new room' },
      description: { type: 'string', default: '' },
      password: { type: 'string', default: '' },
    },
  },
  response: {
    201: roomSchema,
    404: {
      type: 'object',
      properties: { error: { type: 'string' } },
    },
  },
};
