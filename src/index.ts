import { config } from './config';
import { createServer } from './server';

const startServer = async (): Promise<void> => {
  try {
    const server = await createServer();

    server.listen(config.port, (err, address) => {
      if (err) {
        console.error(err);
        process.exit(1);
      }
      console.log(`Server listening at ${address}`);
    });
  } catch (err) {
    console.error(err);
  }
};

void startServer();
