import convict from 'convict';
import * as dotenv from 'dotenv';

dotenv.config({});

const convictConfig = convict({
  frontendUrl: {
    doc: 'frontend url',
    format: String,
    default: 'http://localhost:4200',
    env: 'FRONTEND_URL',
  },
  backendUrl: {
    doc: 'backend url',
    format: String,
    default: 'http://localhost:3000',
    env: 'BACKEND_URL',
  },
  port: {
    doc: 'port',
    format: Number,
    default: 3000,
    env: 'PORT',
  },
  database: {
    connectionString: {
      doc: 'Database connection string',
      format: String,
      default: 'postgresql://user:pw@localhost:5432/chatdb',
      env: 'DATABASE_CONNECTION_STRING',
    },
  },
  testDatabase: {
    connectionString: {
      doc: 'Test database connection string',
      format: String,
      default: 'postgresql://test:test@localhost:5430/test',
      env: 'TEST_DATABASE_CONNECTION_STRING',
    },
  },
  secureSessionKey: {
    doc: 'fastify secure session key',
    format: String,
    default: 'af8d09aa552f175425da11f75263b87812d41e389e48d5500a704476b9b5bab2',
    env: 'SECURE_SESSION_KEY',
  },
  databaseName: {
    doc: 'postgres database name',
    format: String,
    default: 'chatdb',
    env: 'POSTGRES_DATABASE_NAME',
  },
});

convictConfig.validate({ allowed: 'strict' });

export const config = convictConfig.getProperties();
