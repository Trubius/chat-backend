import path from 'path';
import { Pool } from 'pg';
import { createDb, migrate } from 'postgres-migrations';

export const resetTestDatabase = async (pool: Pool) => {
  await pool.query('DROP SCHEMA public CASCADE;');
  await pool.query('CREATE SCHEMA public;');
};

export const createTestDatabase = async (pool: Pool) => {
  const client = await pool.connect();
  try {
    await createDb('test', { client });
    const migrationPath = path.join(__dirname, '../..', 'migrations');
    await migrate({client}, migrationPath);
  } catch (error) {
    console.error(error);
    process.exit(1);
  } finally {
    client.release();
  }
};

export const closeTestDatabase = async (pool: Pool) => {
  await pool.end();
};
