import { createActiveUsersHandler } from "./active-users-handler";
​
describe('Active users handler', () => {
  it('should return null if socket not exists', () => {
    const handler = createActiveUsersHandler();
    expect(handler.getUserBySocket('Socket1')).toEqual(undefined);
  });
​
  it('should return User1 if Socket1 exists', () => {
    const handler = createActiveUsersHandler();
    handler.addSocket('Socket1', 'User1');
    expect(handler.getUserBySocket('Socket1')).toEqual('User1');
  });
​
  it('should return undefined if Socket1 not exists after remove', () => {
    const handler = createActiveUsersHandler();
    handler.addSocket('Socket1', 'User1');
    handler.removeSocket('Socket1');
    expect(handler.getUserBySocket('Socket1')).toEqual(undefined);
  });

  it('should return empty array for sockets if User1 not exists after remove', () => {
    const handler = createActiveUsersHandler();
    handler.addSocket('Socket1', 'User1');
    handler.removeSocket('Socket1');
    expect(handler.getSocketsByUser('User1')).toEqual([]);
  });
​
  it('should return multiple sockets to user', () => {
    const handler = createActiveUsersHandler();
    handler.addSocket('Socket1', 'User1');
    handler.addSocket('Socket2', 'User1');
    expect(handler.getSocketsByUser('User1')).toEqual(['Socket1', 'Socket2']);
  });
​
  it('should not return removed sockets to user', () => {
    const handler = createActiveUsersHandler();
    handler.addSocket('Socket1', 'User1');
    handler.addSocket('Socket2', 'User1');
    handler.removeSocket('Socket2');
    expect(handler.getSocketsByUser('User1')).toEqual(['Socket1']);
  });
​
  it('should handle logout/login', () => {
    const handler = createActiveUsersHandler();
    handler.addSocket('Socket1', 'User1');
    handler.addSocket('Socket2', 'User1');
​
    handler.removeSocket('Socket2');
    handler.addSocket('Socket3', 'User1');
​
    expect(handler.getSocketsByUser('User1')).toEqual(['Socket1', 'Socket3']);
  });

  it('should return all active user ids', () => {
    const handler = createActiveUsersHandler();
    handler.addSocket('Socket1', 'User1');
    handler.addSocket('Socket2', 'User1');
    handler.addSocket('Socket1', 'User2');
    handler.addSocket('Socket3', 'User3');
    expect(handler.getActiveUserIds()).toEqual(['User1', 'User2', 'User3']);
  });

  it('should return empty array for active user ids if no user is active', () => {
    const handler = createActiveUsersHandler();
    handler.addSocket('Socket1', 'User1');
    handler.removeSocket('Socket1');
    expect(handler.getActiveUserIds()).toEqual([]);
  });

  it('should return User1 if User2 logouts', () => {
    const handler = createActiveUsersHandler();
    handler.addSocket('Socket1', 'User1');
    handler.addSocket('Socket2', 'User2');
    handler.removeSocket('Socket2');
    expect(handler.getActiveUserIds()).toEqual(['User1']);
  });

  it('should return void if user id is undefined', () => {
    const handler = createActiveUsersHandler();
    expect(handler.removeSocket('Socket1')).toHaveReturned;
  });
})
