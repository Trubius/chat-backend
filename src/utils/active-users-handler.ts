export const createActiveUsersHandler = () => {
  const userMap = new Map<string, Set<string>>();
  const socketMap = new Map<string, string>();

  return {
    getUserBySocket: (socketId: string): string | undefined => {
      return socketMap.get(socketId);
    },

    getSocketsByUser: (userId: string): string[] => {
      const socketSet = userMap.get(userId);

      return socketSet ? [...socketSet.values()] : [];
    },

    getActiveUserIds: (): string[] => {
      return [...userMap.keys()];
    },

    addSocket: (socketId: string, userId: string): void => {
      const socketSet = userMap.get(userId);

      socketMap.set(socketId, userId);
      if (socketSet) {
        socketSet.add(socketId);
        userMap.set(userId, socketSet);
      } else {
        userMap.set(userId, new Set([socketId]));
      }
    },

    removeSocket: (socketId: string): void => {
      const userId = socketMap.get(socketId);

      socketMap.delete(socketId);
      if (userId) {
        const socketSet = userMap.get(userId)!;

        socketSet.delete(socketId);
        if (socketSet.size === 0) {
          userMap.delete(userId);
        } else {
          userMap.set(userId, socketSet);
        }
      }
    }
  }
}
