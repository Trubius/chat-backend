import { FastifySchema } from 'fastify';

export type RouteSchema = FastifySchema & { tags: string[]; summary: string; description: string };
