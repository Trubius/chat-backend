import { Message } from './message';

export interface Room {
  id: string;
  name: string;
  description: string;
  password: string;
}

export interface RoomWithMessages extends Room {
  messages: Message[];
}
