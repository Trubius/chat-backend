import passport from 'fastify-passport';

export const isAuthenticated = passport.authenticate('local', async (request, response) => {
  if (request.user) {
    return Promise.resolve();
  }
  return response.code(401).send({ error: 'UNAUTHORIZED' });
});
